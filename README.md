# VigorOS System Settings Repository

## Scripts and Configurations

### Dinit Services

- **ananicy-cpp**: Manages process niceness (priority) automatically to improve system responsiveness.
- **amdpstate-guided**: <ins>For AMD CPUs</ins>, this script adjusts the P-State (performance state) based on CPU load, optimizing performance and power consumption.
- **sysctl-extra**: Applies additional system settings that are not covered by the default sysctl configurations such as disabling transparent hugepages and optimizing memory management.
- **bpftune**: Automatically tunes network settings by dynamically adjusting BPF (Berkeley Packet Filter) parameters based on current network conditions.
- **optimze-pci-latency**: Reduces audio latency by adjusting PCI latency timers, specifically targeting sound cards for improved audio performance.
- **zram**: Configures zRAM for efficient memory management under high load, using compression to increase available RAM.
- **uksmd**: Enables Kernel Samepage Merging (KSM) to reduce memory usage by merging identical memory pages.

### Modprobe Configurations

- **amdgpu.conf**: Forces the use of the `amdgpu` driver for certain AMD GPUs, enhancing graphics performance and feature support.
- **nvidia.conf**: Configures NVIDIA driver options for performance and power management, including enabling Page Attribute Table (PAT) and Dynamic Power Management.
- **watchdog-blacklist.conf**: Prevents loading of specific watchdog modules that are unnecessary for most systems, reducing resource usage.

### NetworkManager Configurations

- **disable-wifi-powersave.conf**: Disables Wi-Fi power-saving mode to improve network performance and reliability.

### Security and Limits Configurations

- **99-audio.conf**, **99-realtime.conf**: Sets real-time priority and unlimited memory lock for users in the `audio` and `realtime` groups, optimizing for audio processing tasks.
- **99-sync.conf**: Increases the maximum number of open file descriptors, enhancing system capability to handle a large number of simultaneous connections or files.

### Sysctl Configurations

- **99-bore-scheduler.conf**: Provides optional settings for the BORE scheduler, allowing fine-tuning of process scheduling for performance.
- **99-vigoros-settings.conf**: Applies a wide range of system optimizations, from memory management tweaks to network performance enhancements and security settings.

### Udev Rules

- **30-zram.rules**: Prioritizes recompression of idle pages in ZRAM, potentially saving more memory at the cost of CPU usage.
- **40-hpet-permissions.rules**, **99-cpu-dma-latency.rules**: Adjusts permissions for high precision event timers and CPU DMA latency settings, benefiting real-time audio applications.
- **50-sata.rules**: Sets SATA devices to maximum performance mode, reducing latency and increasing throughput for connected storage devices.
- **60-ioschedulers.rules**: Configures I/O schedulers based on the type of storage device (SSD, HDD, NVMe), optimizing disk access performance.
- **69-hdparm.rules**: Disables spindown for HDDs to avoid delays in disk access due to power-saving modes.
- **70-network.rules**: Disables power-saving for network devices to ensure maximum network performance.
- **71-nvidia.rules**: Manages power states for NVIDIA devices, optimizing for performance or power saving as needed.
