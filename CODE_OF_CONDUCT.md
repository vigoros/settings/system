# Code of Conduct

## Our Commitment

We, as members, contributors, and leaders, are committed to fostering an inclusive and respectful community where everyone can participate free from harassment or discrimination. Our commitment extends to all individuals, regardless of age, physical appearance, visible or invisible disabilities, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic background, nationality, personal appearance, race, religion, sexual identity, or orientation.

We pledge to engage in behaviours that promote an open, welcoming, diverse, inclusive, and healthy community.

## Our Guidelines

We uphold the following behaviours to create a positive environment within our community:

- Demonstrating empathy and kindness towards others.
- Respecting diverse opinions, viewpoints, and experiences.
- Providing and graciously receiving constructive feedback.
- Taking responsibility for our actions, apologising when necessary, and learning from our mistakes.
- Prioritising the collective well-being of the community, not just individual interests.

Unacceptable behaviour within our community includes:

- Use of sexualised language, imagery, or any form of sexual advances.
- Engaging in trolling, insults, derogatory comments, or personal and political attacks.
- Harassment, whether public or private.
- Disclosure of others' private information, such as physical or email addresses, without explicit consent.
- Any other conduct that could reasonably be considered inappropriate in a professional setting.

## Enforcement Responsibilities

Community leaders are responsible for clarifying and enforcing these standards of acceptable behaviour. They will take fair and appropriate corrective actions in response to behaviour deemed inappropriate, threatening, offensive, or harmful.

Community leaders have the authority and duty to remove, edit, or reject comments, commits, code, wiki edits, issues, or any contributions that do not align with this Code of Conduct. They will communicate their reasons for moderation decisions when necessary.

## Scope

This Code of Conduct applies to all community spaces and when individuals officially represent the community in public spaces. Examples of representing our community include using an official email address, posting via an official social media account, or acting as an appointed representative at an online or offline event.

## Reporting Violations

Instances of abusive, harassing, or otherwise unacceptable behaviour should be reported to the community leaders responsible for enforcement at [admin@vigoros.org](mailto:admin@vigoros.org). All complaints will be reviewed and investigated promptly and fairly.

All community leaders are obligated to respect the privacy and security of the reporter of any incident.

## Enforcement Guidelines

Community leaders will follow these Community Impact Guidelines to determine consequences for actions violating this Code of Conduct:

### 1. Correction

**Community Impact**: Inappropriate language or other unprofessional or unwelcome behaviour.

**Consequence**: A private, written warning from community leaders, providing clarity on the violation's nature and an explanation of why the behaviour was inappropriate. A public apology may be requested.

### 2. Warning

**Community Impact**: A single incident or a series of actions violating the Code of Conduct.

**Consequence**: A warning with consequences for continued behaviour. This includes refraining from interactions with the individuals involved, including unsolicited interaction with those enforcing the Code of Conduct, for a specified period. This includes avoiding interactions in community spaces and external channels like social media. Violating these terms may lead to a temporary or permanent ban.

### 3. Temporary Ban

**Community Impact**: A serious violation of community standards, including sustained inappropriate behaviour.

**Consequence**: A temporary ban from any form of interaction or public communication with the community for a specified period. No public or private interaction with the individuals involved, including unsolicited interaction with those enforcing the Code of Conduct, is allowed during this period. Violating these terms may lead to a permanent ban.

### 4. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community standards, including sustained inappropriate behaviour, harassment of an individual, or aggression toward or disparagement of groups of individuals.

**Consequence**: A permanent ban from any form of public interaction within the community.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org), version 2.0, available at [https://www.contributor-covenant.org/version/2/0/code_of_conduct.html](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).

The Community Impact Guidelines were inspired by [Mozilla's code of conduct enforcement ladder](https://github.com/mozilla/diversity).

For answers to common questions about this code of conduct, see the FAQ at [https://www.contributor-covenant.org/faq](https://www.contributor-covenant.org/faq). Translations are available at [https://www.contributor-covenant.org/translations](https://www.contributor-covenant.org/translations).