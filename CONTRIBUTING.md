# Contribution Guidelines

Before contributing to this repository, we encourage you to initiate a discussion regarding your proposed changes. This can be done through issues, email, or any other method to communicate with the repository owners.

Please be aware that we have a Code of Conduct in place, and we kindly request that you adhere to it in all your interactions with this project.

## Pull Request Process

To ensure a smooth contribution process, please follow these steps:

1. Confirm that no one has already made the same changes you are proposing.
2. Verify that your changes do not introduce compilation errors.
3. Provide a detailed description of the changes you have made. This helps us review your Pull Request effectively.
4. Wait for approval from one of the administrators. They may also offer suggestions for improvements.

## Reporting Errors

When reporting errors or issues, please follow these guidelines:

1. Check if someone has already reported the same error. If so, consider joining the existing discussion instead of opening a new ticket.
2. Include all relevant logs and provide a clear description of the situation in which the error occurred.
3. Actively follow any recommendations provided by project supervisors and provide updates on your progress.
4. Promptly inform the community when you have resolved the error and close the related thread.

## Code of Conduct

Please familiarize yourself with our [Code of Conduct](https://github.com/VigorOS/VigorOS-settings/blob/master/CODE_OF_CONDUCT.md) to ensure a respectful and inclusive community environment.

## Contact Us

If you have any questions or need assistance, feel free to reach out to us via email:

- **Michael Bolden / SM9(); :** [michael@vigoros.org](mailto:michael@vigoros.org)

You can also engage in discussions on the following platforms:

- **Discord:** [https://discord.gg/4eWbzp89VD](https://discord.gg/4eWbzp89VD)
- **Telegram:** [https://t.me/+Q5YkCKraxCEwMTdk](https://t.me/+Q5YkCKraxCEwMTdk)