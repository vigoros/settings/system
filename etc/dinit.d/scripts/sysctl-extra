#!/usr/bin/env bash
#
# Copyright (C) VigorOS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

# This script applies additional system settings to improve performance and stability not covered by sysctl.d.

set -euo pipefail

source "/usr/lib/vigoros/common"

declare -A settings=(
    ["/sys/class/rtc/rtc0/max_user_freq"]="3072|3072"                                                          # Set RTC max user frequency to 3072 Hz
    ["/proc/sys/dev/hpet/max-user-freq"]="3072|3072"                                                           # Set HPET max user frequency to 3072 Hz
    ["/sys/kernel/mm/lru_gen/enabled"]="5|0x0005"                                                              # Enable LRU_GEN for improved memory management
    ["/sys/kernel/mm/transparent_hugepage/enabled"]="never|always madvise [never]"                             # Disable transparent hugepages
    ["/sys/kernel/mm/transparent_hugepage/shmem_enabled"]="never|always within_size advise [never] deny force" # Disable transparent hugepages for shared memory
    ["/sys/kernel/mm/transparent_hugepage/defrag"]="never|always defer defer+madvise madvise [never]"          # Disable transparent hugepage defragmentation
    ["/sys/kernel/mm/transparent_hugepage/khugepaged/defrag"]="0|0"                                            # Disable khugepaged defragmentation
)

for setting_path in "${!settings[@]}"; do
    IFS='|' read -r desired_value expected_value <<<"${settings[${setting_path}]}"
    set_sysfs_value "${setting_path}" "${desired_value}" "${expected_value}"
done

exit 0
