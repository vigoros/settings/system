#!/usr/bin/env bash
#
# Copyright (C) VigorOS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

# This script optimizes the performance and minimizes audio latency for sound cards by setting the PCI latency timer to a
# balanced value of 80 cycles. The default values of the latency timer for other PCI devices are reset to prevent any sound gaps caused by
# devices with high latency values.

set -euo pipefail

source "/usr/lib/vigoros/common"

# Set Latency Timers for PCI Devices
# The 'latency timer' for all PCI devices, excluding the host bridge, is set to 20 to regulate their bus utilization. The latency timer,
# expressed in cycles, determines the contiguous cycles a device is permitted to occupy the bus. By reducing this value, audio gaps induced
# by high-latency devices can be alleviated.
set_pci_latency_timer '*:*' 20

# Reset Host Bridge's Latency Timer
# Reverting the host bridge's latency timer to its default value of 0 helps to avoid degradation in DMA performance. Given its role in
# managing communications amongst various computer components, the host bridge should not dominate the PCI bus, hence a zero value is set
# for its latency timer.
set_pci_latency_timer '0:0' 0

# Tune Sound Card's Latency Timer
# The sound devices' latency timer is set to an optimal value of 80, granting them prioritized bus usage. The value 80 strikes a balance,
# ensuring that sound devices have ample time on the PCI bus to deliver high-quality audio output without obstructing other system functions.
for card in ${SOUND_CARDS}; do
    set_pci_latency_timer "${card}" "80"
done

exit 0
